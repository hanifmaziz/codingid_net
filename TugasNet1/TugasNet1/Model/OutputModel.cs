﻿namespace TugasNet1.Model
{
	public class OutputModel
	{
		public int? pk_users_id { get; set; }
		public string? name { get; set; }
		public List<TaskModel>? tasks { get; set; }
	}
}
