﻿namespace TugasNet1.Model
{
	public class UserModelAuth
	{
		public string? UserName { get; set; }
		public string? Password { get; set; }
		public string? Email { get; set; }
		public string? Role { get; set; }
		public string? Surname { get; set; }
		public string? GivenName { get; set; }
	}
}
