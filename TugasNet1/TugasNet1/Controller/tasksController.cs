﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TugasNet1.Model;
using System.Data.SqlClient;
using System.Data;

namespace TugasNet1.Controller
{
	[Route("api/[controller]")]
	[ApiController]
	public class tasksController : ControllerBase
	{
		private readonly IConfiguration _configuration;
		public tasksController(IConfiguration configuration)
		{
			_configuration = configuration;
		}
		[HttpGet]
		[Route("GetUserWithTask")]
		public List<OutputModel> GetUserWithTask(string name)
		{
			List<OutputModel> output = new List<OutputModel>();

			using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
			{
				conn.Open();
				string queryUser = "Select * from Users";
				SqlCommand cmd = new SqlCommand(queryUser, conn);
				if (!string.IsNullOrEmpty(name))
				{
					queryUser = "Select * from Users where name='@name'";
					cmd = new SqlCommand(queryUser, conn);
					cmd.Parameters.AddWithValue("@name", name);
				}
				SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmd);
				DataTable dtUser = new DataTable();
				sqlDataAdapter.Fill(dtUser);

				string queryTasks = "Select * from Tasks";
				SqlCommand cmdTasks = new SqlCommand(queryTasks, conn);
				sqlDataAdapter = new SqlDataAdapter(cmdTasks);
				DataTable dtTasks = new DataTable();
				sqlDataAdapter.Fill(dtTasks);

				for (int i = 0; i < dtUser.Rows.Count; i++)
				{
					OutputModel oModel = new OutputModel();
					oModel.pk_users_id = Convert.ToInt32(dtUser.Rows[i]["pk_users_id"].ToString());
					//oModel.Name = dtUser.Rows[i]["name"].ToString();
					List<TaskModel> listTask = new List<TaskModel>();

					for (int j = 0; j < dtTasks.Rows.Count; j++)
					{
						if (dtUser.Rows[i]["pk_users_id"].ToString() == dtUser.Rows[j]["fk_users_id"].ToString())
						{
							TaskModel tModel = new TaskModel();
							tModel.pk_tasks_id = Convert.ToInt32(dtUser.Rows[j]["pk_tasks_id"].ToString());
							tModel.task_detail = dtUser.Rows[j]["task_detail"].ToString();
							listTask.Add(tModel);
						}
					}
					oModel.tasks = listTask;
					output.Add(oModel);
				}


			}


			return output;
		}
	}
}
