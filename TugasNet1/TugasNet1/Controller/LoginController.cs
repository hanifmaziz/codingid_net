﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using TugasNet1.Model;

namespace TugasNet1.Controller
{
	[Route("api/[controller]")]
	[ApiController]
	public class LoginController : ControllerBase
	{
		private readonly IConfiguration _configuration;
		public LoginController(IConfiguration configuration)
		{
			_configuration = configuration;
		}
		[AllowAnonymous]
		[HttpPost]
		public IActionResult Login([FromBody] UserLogin userLogin)
		{
			var user = Authenticate(userLogin);

			if(user != null)
			{
				var token = Generate(user);
				return Ok(token);
			}

			return NotFound("User Not FOund");
		}

		private string Generate(UserModelAuth user)
		{
			var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
			var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

			var token = new JwtSecurityToken(_configuration["Jwt:issuer"],
				_configuration["Jwt:Audience"],
				null,
				expires: DateTime.Now.AddMinutes(3),
				signingCredentials: credentials
				);

			return new JwtSecurityTokenHandler().WriteToken(token);

		}
		private UserModelAuth Authenticate(UserLogin userLogin)
		{
			var currentUser = UserConstant.Users.FirstOrDefault(o => o.UserName.ToLower() == userLogin.UserName.ToLower() && o.Password == userLogin.Password);

			if(currentUser != null)
			{
				return currentUser;
			}

			return null;
		}

	}
}
